#CREATE SCHEMA `authentication` DEFAULT CHARACTER SET big5 ;
# authentication schema

# --- !Ups

CREATE TABLE Users (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    uuid CHAR(38) NOT NULL,
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    createdOn datetime NOT NULL,
    PRIMARY KEY (id)
);

CREATE UNIQUE INDEX Ix_Users_uuid ON Users (uuid);
CREATE UNIQUE INDEX Ix_Users_email ON Users (email);

# --- !Downs

DROP TABLE Users;