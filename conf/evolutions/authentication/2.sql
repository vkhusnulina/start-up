# --- !Ups

ALTER TABLE Users ADD COLUMN verified BIT;

CREATE TABLE UserVerifications (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    uuid CHAR(38) NOT NULL,
    userId bigint(20) NOT NULL,
    verificationHash varchar(255) NOT NULL,
    validatedOn datetime NULL,
    createdOn datetime NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_UserVerifications_Users FOREIGN KEY (userId) REFERENCES Users(id)
);

CREATE UNIQUE INDEX Ix_UserVerifications_uuid ON UserVerifications (uuid);
CREATE UNIQUE INDEX Ix_UserVerifications_verificationHash ON UserVerifications (verificationHash);

CREATE TABLE Logins (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    uuid CHAR(38) NOT NULL,
    userId bigint(20) NOT NULL,
    createdOn datetime NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_Logins_Users FOREIGN KEY (userId) REFERENCES Users(id)
);

CREATE UNIQUE INDEX Ix_Logins_uuid ON Logins (uuid);

# --- !Downs

ALTER TABLE Users DROP COLUMN verified;

DROP TABLE UserVerifications;
DROP TABLE Logins;