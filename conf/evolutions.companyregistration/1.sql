# CREATE SCHEMA `companyregistration` DEFAULT CHARACTER SET big5 ;
# companyregistration schema

# --- !Ups

CREATE TABLE Companies (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    uuid CHAR(38) NOT NULL,
    companyName varchar(255) NOT NULL,
    createdOn datetime NOT NULL,
    PRIMARY KEY (id)
);

CREATE UNIQUE INDEX Ix_Companies_uuid ON Companies (uuid);
CREATE UNIQUE INDEX Ix_Companies_companyName ON Companies (companyName);

CREATE TABLE PrimaryEmployees (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    uuid CHAR(38) NOT NULL,
    companyId bigint(20) NOT NULL,
    employeeId bigint(20) NOT NULL,
    createdOn datetime NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_PrimaryEmployees_Companies FOREIGN KEY (companyId) REFERENCES Companies(id),
    CONSTRAINT fk_PrimaryEmployees_Employees FOREIGN KEY (employeeId) REFERENCES Employees(id)
);

CREATE UNIQUE INDEX Ix_Users_companyId_employeeId ON PrimaryEmployees (companyId, employeeId);

CREATE TABLE Employees (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    uuid CHAR(38) NOT NULL,
    email varchar(255) NOT NULL,
    fullName varchar(255) NOT NULL,
    companyId bigint(20) NOT NULL,
    createdOn datetime NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_Employees_Companies FOREIGN KEY (companyId) REFERENCES Companies(id)
);

CREATE UNIQUE INDEX Ix_Employees_uuid ON Employees (uuid);
CREATE UNIQUE INDEX Ix_Employees_email ON Employees (email);

CREATE TABLE Invitations (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    uuid CHAR(38) NOT NULL,
    companyId bigint(20) NOT NULL,
    employeeId bigint(20) NOT NULL,
    createdOn datetime NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_Invitations_Companies FOREIGN KEY (companyId) REFERENCES Companies(id),
    CONSTRAINT fk_Invitations_Employees FOREIGN KEY (employeeId) REFERENCES Employees(id)
);

CREATE UNIQUE INDEX Ix_Invitations_uuid ON Invitations (uuid);

# --- !Downs

DROP TABLE Companies;
DROP TABLE PrimaryEmployees;
DROP TABLE Employees;
DROP TABLE Invitations;