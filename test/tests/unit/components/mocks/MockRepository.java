package tests.unit.components.mocks;

import components.common.domain.PsEntity;
import components.common.domain.Repository;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MockRepository <TEntity extends PsEntity> implements Repository<TEntity> {
    List<TEntity> entities;

    public MockRepository() {
        entities = new ArrayList<TEntity>();
    }

    @Override
    public TEntity getById(long tId) {
        for (TEntity entity : entities){
            if (entity.id == tId){
                return entity;
            }
        }

        return null;
    }

    @Override
    public TEntity getByUuid(UUID uuid) {
        for (TEntity entity : entities){
            UUID entityUuid = UUID.fromString(entity.uuid);
            if (entityUuid.equals(uuid)){
                return entity;
            }
        }

        return null;
    }

    @Override
    public void add(TEntity tEntity) {
        entities.add(tEntity);
    }

    @Override
    public void update(TEntity tEntity) {

    }

    @Override
    public void delete(TEntity tEntity) {
        entities.remove(tEntity);
    }

    @Override
    public <TProp> List<TEntity> query(TProp propValue, String propName) {
        List<TEntity> matches = new ArrayList<TEntity>();

        for (TEntity entity : entities){
            try {
                Field field = entity.getClass().getDeclaredField(propName);
                field.setAccessible(true);
                Object value = field.get(entity);

                if (value == propValue) {
                    matches.add(entity);
                }
            } catch (ReflectiveOperationException e) {
                e.printStackTrace();
            }
        }

        return matches;
    }
}
