package tests.unit.components.mocks;

import components.authentication.usecases.dtos.VerificationEmail;
import components.authentication.usecases.services.gateways.EmailGateway;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MockEmailGateway implements EmailGateway {
    public List<VerificationEmail> verificationEmails;

    public MockEmailGateway() {
        verificationEmails = new ArrayList<>();
    }

    @Override
    public void SendVerificationEmail(VerificationEmail verificationEmail) {
        verificationEmails.add(verificationEmail);
    }

    public boolean hasEmailFor(UUID userId){
        for (VerificationEmail ve : verificationEmails){
            if (ve.user.uuid == userId) {
                return true;
            }
        }

        return false;
    }
}
