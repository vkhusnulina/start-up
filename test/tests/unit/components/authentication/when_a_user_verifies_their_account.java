package tests.unit.components.authentication;

import components.authentication.usecases.dtos.Login;
import components.authentication.usecases.dtos.User;
import components.authentication.usecases.dtos.SignUp;
import components.authentication.usecases.services.exceptions.AuthenticationException;
import org.junit.Assert;
import org.junit.Test;

public class when_a_user_verifies_their_account extends AuthenticationSpec {

    private SignUp signUp;
    private User user;

    @Override
    protected void arrange() {
    }

    @Override
    protected void act() throws AuthenticationException {
        signUp = signUpService.signUpUser(userUuid, emailAddress, password);
        user = signUpService.verifyUser(signUp.verificationHash);
    }

    @Test
    public void it_should_return_the_users_uuid() {
        Assert.assertEquals(userUuid, user.uuid);
    }

    @Test
    public void it_should_return_the_uers_mail() {
        Assert.assertEquals(emailAddress, user.email);
    }

    @Test
    public void they_can_login() throws AuthenticationException {
        Login login = loginService.loginUser(userUuid, emailAddress, password);

        Assert.assertNotNull(login);
    }
}
