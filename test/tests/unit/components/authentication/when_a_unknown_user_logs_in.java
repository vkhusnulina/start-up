package tests.unit.components.authentication;

import components.authentication.usecases.services.exceptions.AuthenticationException;
import components.authentication.usecases.services.exceptions.InvalidUserException;
import org.junit.Assert;
import org.junit.Test;

public class when_a_unknown_user_logs_in extends AuthenticationSpec {

    private InvalidUserException invalidUserException;

    @Override
    protected void act() throws AuthenticationException {
        try {
            loginService.loginUser(userUuid, emailAddress, password);
        } catch (InvalidUserException e) {
            invalidUserException = e;
        }
    }
    @Test
    public void an_exception_is_thrown() {
        Assert.assertNotNull(invalidUserException);
    }
}
