package tests.unit.components.authentication;
import components.authentication.domain.entities.Login;
import components.authentication.domain.entities.User;
import components.authentication.domain.entities.UserVerification;
import components.authentication.usecases.services.LoginService;
import components.authentication.usecases.services.SignUpService;
import components.authentication.usecases.services.exceptions.AuthenticationException;
import components.authentication.usecases.services.impl.LoginServiceImpl;
import components.authentication.usecases.services.impl.SignUpServiceImpl;
import components.common.domain.Repository;
import tests.unit.components.mocks.MockEmailGateway;
import tests.unit.components.mocks.MockRepository;
import org.junit.Before;

import java.util.UUID;

public abstract class AuthenticationSpec {

    protected SignUpService signUpService;
    protected LoginService loginService;
    protected MockEmailGateway emailGateway;
    protected UUID userUuid;
    protected String emailAddress;
    protected String password;

    public  AuthenticationSpec(){
        Repository<User> userRepo = new MockRepository<>();
        Repository<UserVerification> userVerificationRepository = new MockRepository<>();
        Repository<Login> loginRepository = new MockRepository<>();

        emailGateway = new MockEmailGateway();
        signUpService = new SignUpServiceImpl(userRepo, userVerificationRepository, emailGateway);
        loginService = new LoginServiceImpl(userRepo, loginRepository);

        userUuid = UUID.randomUUID();
        emailAddress = UUID.randomUUID().toString();
        password = UUID.randomUUID().toString();
    }

    @Before
    public void before() {
        arrange();

        try {
            act();
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }
    }

    protected void arrange(){

    }

    protected void act() throws AuthenticationException{

    }
}


