package tests.unit.components.authentication;

import components.authentication.usecases.dtos.SignUp;
import components.authentication.usecases.dtos.User;
import components.authentication.usecases.services.exceptions.AuthenticationException;
import components.authentication.usecases.services.exceptions.InvalidPasswordException;
import components.authentication.usecases.services.exceptions.InvalidUserException;
import components.authentication.usecases.services.exceptions.UserNotVerifiedException;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class when_a_user_logs_in_with_an_invalid_password extends AuthenticationSpec {

    private SignUp signUp;
    private User user;
    private InvalidPasswordException invalidPasswordException;

    @Override
    protected void arrange() {
    }

    @Override
    protected void act() throws AuthenticationException {
        signUp = signUpService.signUpUser(userUuid, emailAddress, password);
        user = signUpService.verifyUser(signUp.verificationHash);

        try {
            loginService.loginUser(user.uuid, emailAddress, "badpassword");
        } catch (InvalidPasswordException e) {
            invalidPasswordException = e;
        }
    }

    @Test
    public void an_exception_is_thrown() {
        Assert.assertNotNull(invalidPasswordException);
    }
}
