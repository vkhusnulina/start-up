package tests.unit.components.authentication;

import components.authentication.usecases.dtos.SignUp;
import components.authentication.usecases.services.exceptions.AuthenticationException;
import components.authentication.usecases.services.exceptions.UserNotVerifiedException;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class when_a_user_signs_up extends AuthenticationSpec {

    private SignUp signUp;

    @Override
    protected void arrange() {
    }

    @Override
    protected void act() throws AuthenticationException {
        signUp = signUpService.signUpUser(userUuid, emailAddress, password);
    }

    @Test
    public void it_should_generate_a_user_verification_id() {
        Assert.assertNotNull(signUp.userVerificationUuid);
    }

    @Test
    public void it_should_generate_a_verification_hash() {
        Assert.assertNotNull(signUp.verificationHash);
    }

    @Test
    public void it_should_send_the_user_verifiaction_email() {
        Assert.assertTrue(emailGateway.hasEmailFor(userUuid));
    }

    @Test
    public void it_should_prevent_the_user_from_logging_in() {
        try {
            loginService.loginUser(UUID.randomUUID(), emailAddress, password);
        } catch (AuthenticationException e) {
            Assert.assertEquals(e.getClass(), UserNotVerifiedException.class);
        }
    }
}
