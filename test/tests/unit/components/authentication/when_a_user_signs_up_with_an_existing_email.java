package tests.unit.components.authentication;

import components.authentication.usecases.dtos.SignUp;
import components.authentication.usecases.services.exceptions.AuthenticationException;
import components.authentication.usecases.services.exceptions.UserExistsException;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class when_a_user_signs_up_with_an_existing_email extends AuthenticationSpec {

    private UserExistsException userExistsException;

    @Override
    protected void arrange() {
    }

    @Override
    protected void act() throws AuthenticationException {
        signUpService.signUpUser(userUuid, emailAddress, password);

        try {
            signUpService.signUpUser(UUID.randomUUID(), emailAddress, UUID.randomUUID().toString());
        } catch (UserExistsException e){
            userExistsException = e;
        }
    }

    @Test
    public void an_exception_is_thrown() {
        Assert.assertNotNull(userExistsException);
    }
}
