package tests.unit.components.companyregistration.usecases.services;

public class RegistrationServiceTests {

    public class when_a_valid_company_is_registered {

        public void it_should_add_the_company(){

        }
    }

    public class when_a_duplicate_company_is_registered {

        public void it_should_throw_an_exception(){

        }
    }

    public class when_a_company_is_registered_with_an_invalid_email_address {

        public void it_should_throw_an_exception(){

        }
    }

    public class when_team_members_are_invited {

        public void it_should_send_the_invitations_by_email(){

        }
    }

    public class when_a_team_member_with_an_invalid_email_address_is_invited {

        public void it_should_throw_an_exception(){

        }
    }
}
