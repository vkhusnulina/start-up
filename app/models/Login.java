package models;

import play.data.validation.Constraints;

/**
 * Created by brian on 20/09/15.
 */
public class Login {

    @Constraints.Required
    public String username;

    @Constraints.Required
    public String password;
}
