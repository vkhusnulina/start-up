package models;

import play.data.validation.Constraints;

public class Register {

    @Constraints.Required
    public String fullName;

    @Constraints.Required
    public String company;

    @Constraints.Required
    public String email;

    @Constraints.Required
    public String password;
}
