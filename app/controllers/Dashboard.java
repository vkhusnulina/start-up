package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Http.RequestBody;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.*;
import views.html.*;
import views.html.auth.*;

public class Dashboard extends ControllerBase {

	public Result index() {
		String user = session("connected");
		if (isAuth()) {
			return ok("Welcome " + user);
		} else {
			return restrictAccess();
		}
	}
}
