package controllers;

import play.mvc.Controller;
import play.mvc.Result;

public class ControllerBase extends Controller{

	public boolean isAuth(){
		String user = session("connected");
	    if(user != null) {
	        return true;
	    } else {
	    	return false;
	    }
	}
	
	public Result restrictAccess(){
		return redirect("/login");
	}
}
