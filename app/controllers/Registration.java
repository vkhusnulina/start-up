package controllers;

import components.authentication.domain.entities.User;
import components.authentication.domain.entities.UserVerification;
import components.authentication.usecases.services.exceptions.AuthenticationException;
import components.authentication.usecases.services.SignUpService;
import components.authentication.usecases.services.impl.SignUpServiceImpl;
import components.common.domain.Repository;
import components.companyregistration.domain.entities.Company;
import components.companyregistration.domain.entities.Employee;
import components.companyregistration.domain.entities.PrimaryEmployee;
import components.companyregistration.usecases.services.RegistrationService;
import components.companyregistration.usecases.services.exceptions.CompanyRegistrationException;
import components.companyregistration.usecases.services.impl.RegistrationServiceImpl;
import infrastructure.repositories.Jpa.authentication.AuthenticationRepository;
import infrastructure.gateways.EmailGatewayImpl;
import infrastructure.repositories.Jpa.companyregistration.CompanyRegistrationRepository;
import play.Logger;
import play.db.jpa.Transactional;
import play.mvc.Result;
import play.data.Form;
import java.util.UUID;

public class Registration extends ControllerBase {
	private SignUpService signUpService;
	private RegistrationService registrationService;

	public Registration() {
		Repository<User> userRepo = new AuthenticationRepository(User.class);
		Repository<UserVerification> userVerificationRepository = new AuthenticationRepository(UserVerification.class);
		EmailGatewayImpl emailGateway = new EmailGatewayImpl();
		this.signUpService = new SignUpServiceImpl(userRepo, userVerificationRepository, emailGateway);

		Repository<Company> companyRepository = new CompanyRegistrationRepository(Company.class);
		Repository<Employee> employeeRepository = new CompanyRegistrationRepository(Employee.class);
		Repository<PrimaryEmployee> primaryEmployeeRepository = new CompanyRegistrationRepository(PrimaryEmployee.class);
		this.registrationService = new RegistrationServiceImpl(companyRepository, employeeRepository, primaryEmployeeRepository);
	}

	@Transactional(readOnly=true)
	public Result index() {
		return ok(views.html.auth.register.render());
	}

	@Transactional
	public Result register() {
		Logger.info("Registering User");

		models.Register register = Form.form(models.Register.class).bindFromRequest().get();
		
		UUID userId = UUID.randomUUID();

		try {
			signUpService.signUpUser(userId, register.email, register.password);
		} catch (AuthenticationException e) {
			e.printStackTrace();
			return internalServerError();
		}

		components.companyregistration.usecases.dto.Company company = new components.companyregistration.usecases.dto.Company(UUID.randomUUID(), register.company);
		components.companyregistration.usecases.dto.Employee employee = new components.companyregistration.usecases.dto.Employee(userId, register.fullName, register.email);

		try {
			registrationService.registerCompany(company, employee);
		}
		catch (CompanyRegistrationException e){
			e.printStackTrace();
			return internalServerError();
		}

		flash("message_title", "Welcome " + register.fullName);
		flash("message_body",
				"We sent you an activation link to verify your email account.");

		return redirect(routes.Auth.index());
	}

	public Result verify(String hash) {
		components.authentication.usecases.dtos.User user;

		try {
			user = signUpService.verifyUser(hash);
		} catch (AuthenticationException e){
			e.printStackTrace();
			return internalServerError();
		}

		components.companyregistration.usecases.dto.Employee employee = registrationService.getEmployee(user.uuid);

		flash("message_title", "Welcome " + employee.fullName);
		flash("message_body", "Your account was activated successfully! ("
				+  hash + ")");

		return ok(views.html.auth.message.render());
	}

}
