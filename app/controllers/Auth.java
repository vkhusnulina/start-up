package controllers;

import components.authentication.domain.entities.User;
import components.authentication.domain.entities.Login;
import components.authentication.usecases.services.exceptions.AuthenticationException;
import components.authentication.usecases.services.LoginService;
import components.authentication.usecases.services.impl.LoginServiceImpl;
import components.common.domain.Repository;
import infrastructure.repositories.Jpa.authentication.AuthenticationRepository;
import play.*;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.*;

import java.util.UUID;

public class Auth extends ControllerBase {
	private LoginService loginService;

    public Auth() {
        // Todo: Use dependency injection

		Repository<User> userRepo = new AuthenticationRepository(User.class);
		Repository<Login> loginRepository = new AuthenticationRepository(Login.class);
		this.loginService = new LoginServiceImpl(userRepo, loginRepository);
    }

	@Transactional(readOnly=true)
    public Result index() {
		return ok(views.html.auth.login.render());
	}

	@Transactional
	public Result login() {

		Logger.info("Login User");

		models.Login login = Form.form(models.Login.class).bindFromRequest().get();
        UUID loginId = UUID.randomUUID();

		try {
			loginService.loginUser(loginId, login.username, login.password);
		} catch (AuthenticationException e) {
			e.printStackTrace();
			return internalServerError();
		}

		session("connected", loginId.toString());

		return redirect(routes.Dashboard.index());
	}

	public Result logout(){
		session().clear();
		return redirect("/login");
	}

	@Transactional
	public Result forgot() {
		return ok(views.html.auth.forgot.render());
	}


}
