package controllers;

import play.*;
import play.mvc.*;

import views.html.*;
import views.html.rapido.*;

public class Application extends Controller {

    public Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public Result test() {
    	return ok(views.html.rapido.pages_blank_page.render());
    }

    public Result jsmessage() {
        return ok(jsmessage.render("Java Script Message.", "Hello World"));
    }


}
