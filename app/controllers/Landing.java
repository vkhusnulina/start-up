package controllers;

import play.mvc.Result;

public class Landing extends ControllerBase {

	public Result index() {
		return ok(views.html.landing.main.render());
	}

}
