package components.common.domain;

import java.util.List;
import java.util.UUID;

public interface Repository<TEntity extends PsEntity> {

    TEntity getById(long id);
    TEntity getByUuid(UUID uuid);
    void add(TEntity entity);
    void update(TEntity entity);
    void delete(TEntity entity);
    <TProp> List<TEntity> query(TProp propValue, String propName);
}
