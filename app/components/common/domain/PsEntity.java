package components.common.domain;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public abstract class PsEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public long id;

    public String uuid;

    public Date createdOn;
}
