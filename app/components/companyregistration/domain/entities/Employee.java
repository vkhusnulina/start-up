package components.companyregistration.domain.entities;

import components.common.domain.PsEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="Employees", schema="companyregistration")
public class Employee extends PsEntity {
    public String email;

    public String fullName;

    @ManyToOne(fetch=FetchType.LAZY, targetEntity=Company.class)
    @JoinColumn(name="companyId")
    public Company company;

    public Employee() {
    }

    public Employee(String userUuid, String email, String fullName, Company company) {
        this.uuid = userUuid;
        this.email = email;
        this.fullName = fullName;
        this.company = company;
        createdOn = new Date();
    }
}
