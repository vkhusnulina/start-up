package components.companyregistration.domain.entities;

import components.common.domain.PsEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="Companies", schema="companyregistration")
public class Company extends PsEntity {
    public String companyName;

    @OneToMany(mappedBy="company", fetch= FetchType.LAZY)
    public List<PrimaryEmployee> primaryEmployees;

    @OneToMany(mappedBy="company", fetch= FetchType.LAZY)
    public List<Employee> employees;

    @OneToMany(mappedBy="company", fetch= FetchType.LAZY)
    public List<Invitation> invitations;

    public Company() {
    }

    public Company(String companyUuid, String companyName) {
        this.uuid = companyUuid;
        this.companyName = companyName;
        createdOn = new Date();

        primaryEmployees = new ArrayList<>();
        employees = new ArrayList<>();
        invitations = new ArrayList<>();
    }
}
