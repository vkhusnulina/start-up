package components.companyregistration.domain.entities;

import components.common.domain.PsEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="Invitations", schema="companyregistration")
public class Invitation extends PsEntity {
    @ManyToOne(fetch= FetchType.LAZY, targetEntity=Company.class)
    @JoinColumn(name="companyId")
    public Company company;

    @OneToOne(fetch=FetchType.LAZY, targetEntity=Employee.class)
    @JoinColumn(name="employeeId")
    public Employee employee;

    public Invitation() {
    }

    public Invitation(String invitationUuid, Company company, Employee employee) {
        this.uuid = invitationUuid;
        this.company = company;
        this.employee = employee;
        createdOn = new Date();
    }
}
