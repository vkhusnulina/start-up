package components.companyregistration.domain.entities;

import components.common.domain.PsEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="PrimaryEmployees", schema="companyregistration")
public class PrimaryEmployee extends PsEntity {
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="companyId")
    public Company company;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="employeeId")
    public Employee employee;

    public PrimaryEmployee() {
    }

    public PrimaryEmployee(Company company, Employee employee) {
        this.company = company;
        this.employee = employee;
        createdOn = new Date();
    }
}
