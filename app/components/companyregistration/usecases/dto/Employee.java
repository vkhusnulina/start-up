package components.companyregistration.usecases.dto;

import java.util.UUID;

/**
 * Created by brian on 30/08/15.
 */
public class Employee {

    public UUID userId;
    public String fullName;
    public String emailAddress;

    public Employee(UUID userId, String fullName, String emailAddress) {
        this.userId = userId;
        this.fullName = fullName;
        this.emailAddress = emailAddress;
    }
}
