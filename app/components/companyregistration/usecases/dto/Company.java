package components.companyregistration.usecases.dto;

import java.util.List;
import java.util.UUID;

/**
 * Created by brian on 30/08/15.
 */
public class Company {

    public UUID companyId;
    public String companyName;
    public List<Employee> employees;

    public Company(UUID companyId, String companyName) {
        this.companyId = companyId;
        this.companyName = companyName;
    }
}
