package components.companyregistration.usecases.services;

import components.companyregistration.usecases.dto.Employee;
import components.companyregistration.usecases.dto.Company;
import components.companyregistration.usecases.services.exceptions.CompanyRegistrationException;

import java.util.List;
import java.util.UUID;

/**
 * Created by brian on 30/08/15.
 */
public interface RegistrationService {

    void registerCompany(Company company, Employee primaryEmployee) throws CompanyRegistrationException;
    void registerEmployeeWithCompany(UUID companyId, Employee employee);
    void inviteEmployees(UUID companyId, List<String> emailAddresses);
    Company getCompany(UUID companyId);
    Employee getEmployee(UUID userId);
}
