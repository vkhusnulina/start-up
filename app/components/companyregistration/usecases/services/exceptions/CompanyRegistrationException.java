package components.companyregistration.usecases.services.exceptions;

/**
 * Created by Brian on 02/10/2015.
 */
public class CompanyRegistrationException extends Exception {
    public CompanyRegistrationException() {
    }

    public CompanyRegistrationException(String message) {
        super(message);
    }

    public CompanyRegistrationException(String message, Throwable cause) {
        super(message, cause);
    }
}
