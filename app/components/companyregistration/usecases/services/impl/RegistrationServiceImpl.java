package components.companyregistration.usecases.services.impl;

import components.common.domain.Repository;
import components.companyregistration.domain.entities.Company;
import components.companyregistration.domain.entities.Employee;
import components.companyregistration.domain.entities.PrimaryEmployee;
import components.companyregistration.usecases.services.RegistrationService;
import components.companyregistration.usecases.services.exceptions.CompanyExistsException;
import components.companyregistration.usecases.services.exceptions.CompanyRegistrationException;

import java.util.List;
import java.util.UUID;

public class RegistrationServiceImpl implements RegistrationService {
    private Repository<Company> companyRepository;
    private Repository<Employee> employeeRepository;
    private Repository<PrimaryEmployee> primaryEmployeeRepository;

    public RegistrationServiceImpl(Repository<Company> companyRepository, Repository<Employee> employeeRepository, Repository<PrimaryEmployee> primaryEmployeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
        this.primaryEmployeeRepository = primaryEmployeeRepository;
    }

    @Override
    public void registerCompany(components.companyregistration.usecases.dto.Company company, components.companyregistration.usecases.dto.Employee primaryEmployee) throws CompanyRegistrationException {
        if (companyExists(company.companyName)){
            throw new CompanyExistsException();
        }

        if (employeeExists(primaryEmployee.emailAddress)){
            throw new CompanyExistsException();
        }

        Company companyEntity = addCompany(company);
        Employee employeeEntity = addEmployee(primaryEmployee, companyEntity);
        addPrimaryEmployee(companyEntity, employeeEntity);
    }

    private boolean companyExists(String companyName) {
        return companyRepository.query(companyName, "companyName").size() > 0;
    }

    private boolean employeeExists(String emailAddress) {
        return employeeRepository.query(emailAddress, "email").size() > 0;
    }

    private Company addCompany(components.companyregistration.usecases.dto.Company company) {
        Company companyEntity = new Company(company.companyId.toString(), company.companyName);
        companyRepository.add(companyEntity);
        return companyEntity;
    }

    private Employee addEmployee(components.companyregistration.usecases.dto.Employee primaryEmployee, Company companyEntity) {
        Employee employeeEntity = new Employee(primaryEmployee.userId.toString(), primaryEmployee.emailAddress, primaryEmployee.fullName, companyEntity);
        employeeRepository.add(employeeEntity);
        return employeeEntity;
    }

    private PrimaryEmployee addPrimaryEmployee(Company companyEntity, Employee employeeEntity) {
        PrimaryEmployee primaryEmployeeEntity = new PrimaryEmployee(companyEntity, employeeEntity);
        primaryEmployeeRepository.add(primaryEmployeeEntity);
        return primaryEmployeeEntity;
    }

    @Override
    public void registerEmployeeWithCompany(UUID companyId, components.companyregistration.usecases.dto.Employee employee) {

    }

    @Override
    public void inviteEmployees(UUID companyId, List<String> emailAddresses) {

    }

    @Override
    public components.companyregistration.usecases.dto.Company getCompany(UUID companyId) {
        return null;
    }

    @Override
    public components.companyregistration.usecases.dto.Employee getEmployee(UUID userId) {
        Employee employee = employeeRepository.getByUuid(userId);

        return new components.companyregistration.usecases.dto.Employee(UUID.fromString(employee.uuid), employee.fullName, employee.email);
    }
}
