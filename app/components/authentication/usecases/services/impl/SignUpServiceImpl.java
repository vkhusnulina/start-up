package components.authentication.usecases.services.impl;

import components.authentication.domain.entities.User;
import components.authentication.domain.entities.UserVerification;
import components.authentication.usecases.dtos.SignUp;
import components.authentication.usecases.dtos.VerificationEmail;
import components.authentication.usecases.services.exceptions.AuthenticationException;
import components.authentication.usecases.services.SignUpService;
import components.authentication.usecases.services.exceptions.UserExistsException;
import components.authentication.usecases.services.exceptions.UserVerificationException;
import components.authentication.usecases.services.gateways.EmailGateway;
import components.authentication.usecases.services.utils.HashException;
import components.authentication.usecases.services.utils.Hasher;
import components.common.domain.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class SignUpServiceImpl implements SignUpService {

    private Repository<User> userRepository;
    private Repository<UserVerification> userVerificationRepository;
    private EmailGateway emailGateway;

    public SignUpServiceImpl(Repository<User> userRepository, Repository<UserVerification> userVerificationRepository, EmailGateway emailGateway) {
        this.userRepository = userRepository;
        this.userVerificationRepository = userVerificationRepository;
        this.emailGateway = emailGateway;
    }

    @Override
    public SignUp signUpUser(UUID userUuid, String emailAddress, String password) throws AuthenticationException {
        if(userExists(emailAddress)) {
            throw new UserExistsException();
        }

        String passwordHash = hashPassword(password);
        User user = addUser(userUuid, emailAddress, passwordHash);

        UUID userVerificationUUid = UUID.randomUUID();
        String userVerificationHash = hashVerification(userUuid, userVerificationUUid);
        addUserVerification(userVerificationUUid, userVerificationHash, user);

        SignUp signUp = new SignUp(userVerificationUUid, userVerificationHash);
        sendVerificationEmail(userUuid, emailAddress, signUp);

        return signUp;
    }

    private boolean userExists(String emailAddress) {
        List<User> users = userRepository.query(emailAddress, "email");

        if (users == null || users.size() <= 0){
            return false;
        }

        return true;
    }

    private void sendVerificationEmail(UUID userUuid, String emailAddress, SignUp signUp) {
        components.authentication.usecases.dtos.User dtoUser = new components.authentication.usecases.dtos.User(userUuid, emailAddress);
        VerificationEmail verificationEmail = new VerificationEmail(dtoUser, signUp);
        emailGateway.SendVerificationEmail(verificationEmail);
    }

    private String hashPassword(String password) throws AuthenticationException {
        try {
            return Hasher.hashPassword(password);
        } catch (HashException e) {
            throw new AuthenticationException("Could not sign up user", e);
        }
    }

    private String hashVerification(UUID userId, UUID userVerificationUuid) throws AuthenticationException {
        try {
            return Hasher.hashVerification(userId, userVerificationUuid);
        } catch (HashException e) {
            throw new AuthenticationException("Could not sign up user", e);
        }
    }

    private User addUser(UUID userId, String emailAddress, String passwordHash) {
        User user = new User(userId.toString(), emailAddress, passwordHash);
        userRepository.add(user);

        return user;
    }

    private UserVerification addUserVerification(UUID userVerificationUuid, String userVerificationHash, User user) {
        UserVerification userVerification = new UserVerification(userVerificationUuid.toString(), userVerificationHash, user);
        userVerificationRepository.add(userVerification);

        return userVerification;
    }

    @Override
    public components.authentication.usecases.dtos.User verifyUser(String userVerificationHash) throws AuthenticationException {
        if (userVerificationHash == null){
            throw new UserVerificationException();
        }

        UserVerification userVerification = getUserVerification(userVerificationHash);

        if (userVerification == null){
            throw new UserVerificationException();
        }

        checkVerification(UUID.fromString(userVerification.uuid), userVerificationHash);

        userVerification.verifiedOn = new Date();
        userVerification.user.verified = true;

        userVerificationRepository.update(userVerification);

        return new components.authentication.usecases.dtos.User(UUID.fromString(userVerification.user.uuid), userVerification.user.email);
    }

    private UserVerification getUserVerification(String userVerificationHash) {
        List<UserVerification> verifications = userVerificationRepository.query(userVerificationHash, "verificationHash");

        if (verifications == null || verifications.size() == 0){
            return null;
        }

        return verifications.get(0);
    }

    private void checkVerification(UUID userVerificationUuid, String userVerificationHash) throws AuthenticationException {
        try {
            if (!Hasher.checkVerification(userVerificationUuid, userVerificationHash)){
                throw new UserVerificationException();
            }
        } catch (HashException e) {
            throw new UserVerificationException();
        }
    }
}
