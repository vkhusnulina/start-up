package components.authentication.usecases.services.impl;

import components.authentication.domain.entities.Login;
import components.authentication.domain.entities.User;
import components.authentication.usecases.services.LoginService;
import components.authentication.usecases.services.exceptions.InvalidPasswordException;
import components.authentication.usecases.services.exceptions.InvalidUserException;
import components.authentication.usecases.services.exceptions.UserNotVerifiedException;
import components.authentication.usecases.services.utils.HashException;
import components.common.domain.Repository;
import components.authentication.usecases.services.exceptions.AuthenticationException;
import components.authentication.usecases.services.utils.Hasher;

import java.util.List;
import java.util.UUID;

public class LoginServiceImpl implements LoginService {

    private Repository<User> userRepository;
    private Repository<Login> loginRepository;

    public LoginServiceImpl(Repository<User> userRepository, Repository<Login> loginRepository) {
        this.userRepository = userRepository;
        this.loginRepository = loginRepository;
    }

    @Override
    public components.authentication.usecases.dtos.Login loginUser(UUID loginUuid, String emailAddress, String password) throws AuthenticationException {
        User user = getUser(emailAddress);

        validateUser(user);
        checkPassword(password, user.password);
        addLogin(loginUuid, user);

        return new components.authentication.usecases.dtos.Login(loginUuid, UUID.fromString(user.uuid));
    }

    private User getUser(String emailAddress) {
        List<User> users = userRepository.query(emailAddress, "email");

        if (users == null || users.size() <= 0){
            return null;
        }

        return users.get(0);
    }

    private void validateUser(User user) throws InvalidUserException, UserNotVerifiedException {
        if (user == null){
            throw new InvalidUserException();
        }

        if (!user.verified){
            throw new UserNotVerifiedException();
        }
    }

    private void checkPassword(String password, String passwordHash) throws AuthenticationException {
        try {
            if (!Hasher.checkPassword(password, passwordHash)){
                throw new InvalidPasswordException();
            }
        } catch (HashException e) {
            throw new InvalidPasswordException();
        }
    }

    private Login addLogin(UUID loginUuid, User user) {
        Login login = new Login(loginUuid.toString(), user);
        loginRepository.add(login);
        return login;
    }
}
