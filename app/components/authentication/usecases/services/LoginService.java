package components.authentication.usecases.services;

import components.authentication.usecases.dtos.Login;
import components.authentication.usecases.services.exceptions.AuthenticationException;

import java.util.UUID;

/**
 * Created by brian on 30/08/15.
 */
public interface LoginService {
    Login loginUser(UUID loginUuid, String emailAddress, String password) throws AuthenticationException;
}
