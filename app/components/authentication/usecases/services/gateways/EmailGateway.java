package components.authentication.usecases.services.gateways;

import components.authentication.usecases.dtos.VerificationEmail;

public interface EmailGateway {
    void SendVerificationEmail(VerificationEmail verificationEmail);
}
