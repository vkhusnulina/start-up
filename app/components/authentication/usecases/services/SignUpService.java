package components.authentication.usecases.services;

import components.authentication.usecases.dtos.SignUp;
import components.authentication.usecases.dtos.User;
import components.authentication.usecases.services.exceptions.AuthenticationException;

import java.util.UUID;

/**
 * Created by brian on 30/08/15.
 */
public interface SignUpService {
    SignUp signUpUser(UUID userUuid, String emailAddress, String password) throws AuthenticationException;
    User verifyUser(String userVerificationHash) throws AuthenticationException;
}
