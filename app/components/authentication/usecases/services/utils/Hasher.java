package components.authentication.usecases.services.utils;

import org.mindrot.jbcrypt.BCrypt;

import java.util.UUID;

/**
 * Created by brian on 12/09/15.
 */
public class Hasher {

    public static String hashPassword(String password) throws HashException {
        if (password == null) {
            throw new HashException("No password defined!");
        }

        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public static String hashVerification(UUID userId, UUID userVerificationId) throws HashException {
        if (userId == null) {
            throw new HashException("No userId defined!");
        }

        if (userVerificationId == null) {
            throw new HashException("No userId defined!");
        }

        return BCrypt.hashpw(userVerificationId.toString(), BCrypt.gensalt());
    }

    public static boolean checkPassword(String password, String hash) throws HashException {
        if (password == null) {
            throw new HashException("No password defined!");
        }

        if (hash == null) {
            throw new HashException("No hash defined!");
        }

        return BCrypt.checkpw(password, hash);
    }

    public static boolean checkVerification(UUID userVerificationId, String hash) throws HashException {
        if (userVerificationId == null) {
            throw new HashException("No userVerificationId defined!");
        }

        if (hash == null) {
            throw new HashException("No hash defined!");
        }

        return BCrypt.checkpw(userVerificationId.toString(), hash);
    }
}
