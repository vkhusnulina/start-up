package components.authentication.usecases.services.utils;

/**
 * Created by brian on 12/09/15.
 */
public class HashException extends Exception {

    public HashException(String message) {
        super(message);
    }

    public HashException() {
    }
}
