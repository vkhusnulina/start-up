package components.authentication.usecases.dtos;

import java.util.UUID;

/**
 * Created by Brian on 05/10/2015.
 */
public class User {
    public UUID uuid;

    public String email;

    public User(UUID uuid, String email) {
        this.uuid = uuid;
        this.email = email;
    }
}
