package components.authentication.usecases.dtos;

public class VerificationEmail {
    public User user;

    public SignUp signUp;

    public VerificationEmail(User user, SignUp signUp) {
        this.user = user;
        this.signUp = signUp;
    }
}
