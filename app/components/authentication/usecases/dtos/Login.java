package components.authentication.usecases.dtos;

import java.util.UUID;

public class Login {

    public UUID userUuid;

    public UUID loginUuid;

    public Login(UUID userUuid, UUID loginUuid) {
        this.userUuid = userUuid;
        this.loginUuid = loginUuid;
    }
}
