package components.authentication.usecases.dtos;

import java.util.UUID;

/**
 * Created by Brian on 03/10/2015.
 */
public class SignUp {
    public UUID userVerificationUuid;

    public String verificationHash;

    public SignUp(UUID userVerificationUuid, String verificationHash) {
        this.userVerificationUuid = userVerificationUuid;
        this.verificationHash = verificationHash;
    }
}
