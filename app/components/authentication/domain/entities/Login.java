package components.authentication.domain.entities;

import components.common.domain.PsEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="Logins", schema="authentication")
public class Login extends PsEntity {

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="userId")
    public User user;

    public Login() {
    }

    public Login(String uuid, User user) {
        this.uuid = uuid;
        this.user = user;
        createdOn = new Date();
    }
}
