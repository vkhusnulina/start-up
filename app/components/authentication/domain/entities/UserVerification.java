package components.authentication.domain.entities;

import components.common.domain.PsEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="UserValidations", schema="authentication")
public class UserVerification extends PsEntity {

    public String verificationHash;

    public Date verifiedOn;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="userId")
    public User user;

    public UserVerification() {
    }

    public UserVerification(String uuid, String verificationHash, User user) {
        this.uuid = uuid;
        this.verificationHash = verificationHash;
        this.user = user;
        createdOn = new Date();
    }
}
