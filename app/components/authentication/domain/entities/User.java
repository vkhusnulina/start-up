package components.authentication.domain.entities;

import components.common.domain.PsEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="Users", schema="authentication")
public class User extends PsEntity {

    public String email;

    public String password;

    public boolean verified;

    public User() {
    }

    public User(String uuid, String emailAddress, String passwordHash) {
        this.uuid = uuid;
        this.email = emailAddress;
        this.password = passwordHash;
        createdOn = new Date();
    }
}
