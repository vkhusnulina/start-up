package infrastructure.repositories.Jpa;

import components.common.domain.PsEntity;
import components.common.domain.Repository;
import play.db.jpa.JPA;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.UUID;

public abstract class RepositoryImpl<TEntity extends PsEntity> implements Repository<TEntity> {

    protected EntityManager entityManager;

    public RepositoryImpl() {
        entityManager = JPA.em(getPersistenceUnit());
    }

    @Override
    public TEntity getById(long id) {
        return entityManager.find(getEntityClass(), id);
    }

    @Override
    public TEntity getByUuid(UUID uuid){
        List<TEntity> entities = query(uuid.toString(), "uuid");

        return entities.get(0);
    }

    @Override
    public void add(TEntity tEntity) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();

        try {
            entityManager.persist(tEntity);
            tx.commit();
        }
        catch (Exception e){
            e.printStackTrace();
            tx.rollback();
            throw e;
        }
    }

    @Override
    public void update(TEntity tEntity) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();

        try {
            entityManager.persist(tEntity);
            tx.commit();
        }
        catch (Exception e){
            e.printStackTrace();
            tx.rollback();
            throw e;
        }
    }

    @Override
    public void delete(TEntity tEntity) {
        entityManager.remove(tEntity);
    }

    @Override
    public <TProp> List<TEntity> query(TProp propValue, String propName){

        Class<TEntity> entityClass = getEntityClass();
        CriteriaBuilder qb = entityManager.getCriteriaBuilder();
        CriteriaQuery<TEntity> c = qb.createQuery(entityClass);
        Root<TEntity> p = c.from(entityClass);
        Predicate condition = qb.equal(p.get(propName), propValue);
        c.where(condition);
        TypedQuery<TEntity> q = entityManager.createQuery(c);

        return q.getResultList();
    }

    protected abstract String getPersistenceUnit();

    protected abstract Class<TEntity> getEntityClass();
}
