package infrastructure.repositories.Jpa.companyregistration;

import components.common.domain.PsEntity;
import infrastructure.repositories.Jpa.PersistenceUnits;
import infrastructure.repositories.Jpa.RepositoryImpl;

public class CompanyRegistrationRepository<TEntity extends PsEntity> extends RepositoryImpl<TEntity> {

    private Class<TEntity> entityClass;

    public CompanyRegistrationRepository(Class<TEntity> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    protected String getPersistenceUnit() {
        return PersistenceUnits.CompanyRegistration;
    }

    @Override
    protected Class<TEntity> getEntityClass() {
        return entityClass;
    }
}
