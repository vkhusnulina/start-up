package infrastructure.repositories.Jpa.authentication;

import components.common.domain.PsEntity;
import infrastructure.repositories.Jpa.PersistenceUnits;
import infrastructure.repositories.Jpa.RepositoryImpl;

public class AuthenticationRepository<TEntity extends PsEntity> extends RepositoryImpl<TEntity> {

    private Class<TEntity> entityClass;

    public AuthenticationRepository(Class<TEntity> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    protected String getPersistenceUnit() {
        return PersistenceUnits.Authentication;
    }

    @Override
    protected Class<TEntity> getEntityClass() {
        return entityClass;
    }
}
