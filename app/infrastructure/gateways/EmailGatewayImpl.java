package infrastructure.gateways;

import components.authentication.usecases.dtos.VerificationEmail;
import components.authentication.usecases.services.gateways.EmailGateway;
import play.Logger;

public class EmailGatewayImpl implements EmailGateway {
    @Override
    public void SendVerificationEmail(VerificationEmail verificationEmail) {
        Logger.info("Sending Verification email");
        Logger.info("userVerificationUuid = " + verificationEmail.signUp.userVerificationUuid);
        Logger.info("userVerificationHash = " + verificationEmail.signUp.verificationHash);
    }
}
